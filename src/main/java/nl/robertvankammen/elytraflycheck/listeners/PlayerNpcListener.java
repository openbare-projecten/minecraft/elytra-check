package nl.robertvankammen.elytraflycheck.listeners;

import com.destroystokyo.paper.event.player.PlayerUseUnknownEntityEvent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.datafixers.util.Pair;
import net.minecraft.network.protocol.game.*;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.Items;
import nl.robertvankammen.elytraflycheck.Main;
import nl.robertvankammen.elytraflycheck.commands.FlyShop;
import nl.robertvankammen.elytraflycheck.domain.ShopInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_20_R1.CraftServer;
import org.bukkit.craftbukkit.v1_20_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_20_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;
import java.util.Objects;

import static java.util.UUID.randomUUID;
import static net.minecraft.world.entity.EquipmentSlot.MAINHAND;
import static net.minecraft.world.entity.EquipmentSlot.OFFHAND;

public class PlayerNpcListener implements Listener {

    private static final String TEXTURES = "textures";

    //https://mineskin.org/gallery
    private static final String SKIN_VALUE = "ewogICJ0aW1lc3RhbXAiIDogMTYxMzIxMTk0NzIyOCwKICAicHJvZmlsZUlkIiA6ICJlNzkzYjJjYTdhMmY0MTI2YTA5ODA5MmQ3Yzk5NDE3YiIsCiAgInByb2ZpbGVOYW1lIiA6ICJUaGVfSG9zdGVyX01hbiIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8zOGI5MWU4ZThmZmZlZWE2YjM0Y2IyMDAwNmJlMzY5MWIzNGZkYzY3NTJlNDBhN2Q5N2JkZjA1MGNhZDdjMWE4IgogICAgfQogIH0KfQ==";
    private static final String SKIN_SIGNATURE = "jVNNe+FlBHAzpLtqKAfGTqwGGxic4pnopTqRLfOW6PMMIbZcfm2DKwp6S1ygTy2/w0XdAjNV3A5L4/UTimFOKhMl4Y5E8q5fTE0UDyWkVv1ylzaHKc52JIGKYMtXrIg15LvwX2kIW5zkbyUyuevlSjuvdXtXrymK4DGt7VBzzGrbFwLUSwNLvsoBrYUyN9ylkPxThZQxhCP4W/R/PJoQUmTysbaU70Nl56xq2e2ZT44eYO3lJ5odPG3oeTupiKmmC6V41xui+lQyGlCSqN0tGD3a9Qo70WpMsrOe/EALj66SoVFoXkGc/NnCk50gJruE+1sXIWUF2/D+zoGuEKQfoSBc0k20ENIA164VCwMKH2qC3OJ7aOHbPJRZWLv/egD8ZrFJ+mCAnLbjuOZSB4681Ymp4YWpNWYe/rJY06aqFeKJcP5cF+xh/ZpaCZvyMtsFODsCFk1NXWyqWzGd48sxNhWEAfqgaJuoIvSbiq2aoxmIEEi4KHV/lP7HpuaRPGnBFjM3WEWBfbE28kN+NMGD8kqq692hVSekoJvou9+rqfgBIhT3CFwDlJNPae0YIjvZhB1juxpDZrrqdqWsKm5sYbGUOMt+Y+U9h4Ukb4UVOOwzJHHjDseRyOLQt15+39hg5I3SNHgJF3h7zdlgY/TopEX4W4mj/dl7RJDAafq4UL4=";
    private ServerPlayer npc;
    private final Main plugin;
    private final FlyShop flyShop;
    private final List<ShopInfo> shopInfos;
    private final double npcX;
    private final double npcY;
    private final double npcZ;

    public PlayerNpcListener(Main main, FlyShop flyShop, List<ShopInfo> shopInfos, Location locationNpc) {
        this.plugin = main;
        this.flyShop = flyShop;
        this.shopInfos = shopInfos;
        npcX = locationNpc.x();
        npcY = locationNpc.y();
        npcZ = locationNpc.z();
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, this::createNPC, 100); // hack zodat de wereld geladen is
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, this::updateNpcRotation, 1, 1);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent joinEvent) {
        if (npc != null && joinEvent.getPlayer().getWorld().getName().equals("world")) {
            addNPCPacket(npc, (CraftPlayer) joinEvent.getPlayer());
        }
    }

    @EventHandler
    public void onNetherjoin(PlayerChangedWorldEvent playerChangedWorldEvent) {
        if (npc != null && playerChangedWorldEvent.getPlayer().getWorld().getName().equals("world")) {
            addNPCPacket(npc, (CraftPlayer) playerChangedWorldEvent.getPlayer());
        }
    }

    @EventHandler
    public void playerInteractEntityEvent(PlayerUseUnknownEntityEvent playerInteractEntityEvent) {
        var player = playerInteractEntityEvent.getPlayer();
        var location = player.getLocation();
        var x = location.getX();
        var y = location.getY();
        var z = location.getZ();
        var worldName = location.getWorld().getName();
        if (worldName.equals("world") && checkSpawnLocation(x, y, z)) {
            var inv = flyShop.openNoFlyShop(player, shopInfos);
            Main.PLAYER_SHOPS.put(player.getUniqueId(), inv);
        }
    }

    private boolean checkSpawnLocation(double x, double y, double z) {
        return x >= npcX - 10 && x <= npcX + 10
                && y >= npcY - 10 && y <= npcY + 10
                && z >= npcZ - 10 && z <= npcZ + 10;
    }

    private double calculateDistance(Player p) {
        var diffX = this.npc.getX() - p.getLocation().getX();
        var diffZ = this.npc.getZ() - p.getLocation().getZ();
        var x = diffX < 0 ? (diffX * -1) : diffX;
        var z = diffZ < 0 ? (diffZ * -1) : diffZ;
        return Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
    }

    public void createNPC() {
        var server = ((CraftServer) Bukkit.getServer()).getServer();
        var world = ((CraftWorld) Bukkit.getWorld("world")).getHandle(); // Change "world" to the world the NPC should be spawned in.
        var gameProfile = getProfiel();
        npc = new ServerPlayer(server, world, gameProfile);
        npc.setPos(npcX, npcY, npcZ);
        setSkinn(gameProfile);
        addNPCPacket(npc);
    }

    public GameProfile getProfiel() {
        return new GameProfile(randomUUID(), "Vader tijd");
    }

    private void setSkinn(GameProfile gameProfile) {
        gameProfile.getProperties().get(TEXTURES).clear();
        gameProfile.getProperties().put(TEXTURES, new Property(TEXTURES, SKIN_VALUE, SKIN_SIGNATURE));
    }

    public void addNPCPacket(ServerPlayer npc) {
        Bukkit.getOnlinePlayers().forEach(player -> addNPCPacket(npc, (CraftPlayer) player));
    }

    public void addNPCPacket(ServerPlayer npc, CraftPlayer player) {
        var connection = player.getHandle().connection;
        npc.getEntityData().set(net.minecraft.world.entity.player.Player.DATA_PLAYER_MODE_CUSTOMISATION, (byte) 126);
        connection.send(new ClientboundPlayerInfoUpdatePacket(ClientboundPlayerInfoUpdatePacket.Action.ADD_PLAYER, npc));
        connection.send(new ClientboundAddPlayerPacket(npc));
        var dataValues = npc.getEntityData().getNonDefaultValues();
        connection.send(new ClientboundSetEntityDataPacket(npc.getId(), dataValues));

        var itemstack = new net.minecraft.world.item.ItemStack(Items.CLOCK);
        var itemstack2 = new net.minecraft.world.item.ItemStack(Items.RECOVERY_COMPASS);
        var equipmentList = List.of(Pair.of(MAINHAND, itemstack), Pair.of(OFFHAND, itemstack2));
        connection.send(new ClientboundSetEquipmentPacket(npc.getId(), equipmentList));

//        Bukkit.getScheduler()
//                .scheduleSyncDelayedTask(plugin, () -> connection.send(
//                        new ClientboundPlayerInfoUpdatePacket(ClientboundPlayerInfoUpdatePacket.Action.REMOVE_PLAYER, npc)), 100);
    }

    private void updateNpcRotation() {
        if (npc == null)
            return;
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (!p.getWorld().getName().equals("world")) continue;
            if (calculateDistance(p) > 5) continue;

            var connection = ((CraftPlayer) p).getHandle().connection;
            var difference = p.getLocation().subtract(npc.getBukkitEntity().getLocation()).toVector().normalize();
            var degrees = (float) Math.toDegrees(Math.atan2(difference.getZ(), difference.getX()) - Math.PI / 2);
            var angle = (byte) ((degrees * 256.0F) / 360.0F);

            var height = npc.getBukkitEntity().getLocation().subtract(p.getLocation()).toVector().normalize();
            var pitch = (byte) ((Math.toDegrees(Math.atan(height.getY())) * 256.0F) / 360.0F);

            connection.send(new ClientboundRotateHeadPacket(npc, angle));
            connection.send(new ClientboundMoveEntityPacket.Rot(npc.getId(), angle, pitch, true));
        }
    }
}
