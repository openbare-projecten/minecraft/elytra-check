package nl.robertvankammen.elytraflycheck.listeners;

import nl.robertvankammen.elytraflycheck.Main;
import nl.robertvankammen.elytraflycheck.domain.PlayerInfo;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.time.LocalDateTime;

public class PlayerAfkListener implements Listener {
    private LocalDateTime waterTimeout;

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent playerMoveEvent) {
        var player = playerMoveEvent.getPlayer();
        var playerInfo = getPlayerInfo(player.getUniqueId().toString());
        if (playerNotMoved(playerMoveEvent) || isPlayerAfk(player)) {
        } else {
            playerInfo.setLastNonAfkMoveEvent(LocalDateTime.now());
        }
        if (player.isGliding() && playerInfo.getMinutenElytraNietGebruikt() > 10) {
            playerInfo.resetElytra();
            System.out.println(player.getUniqueId() + " is gaan vliegen");
        }
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent playerLoginEvent) {
        getPlayerInfo(playerLoginEvent.getPlayer().getUniqueId().toString()).setLastNonAfkMoveEvent(LocalDateTime.now());
    }

    private PlayerInfo getPlayerInfo(String uuid) {
        var playerInfo = Main.PLAYERS.get(uuid);
        if (playerInfo == null) {
            playerInfo = new PlayerInfo(uuid);
            Main.PLAYERS.put(uuid, playerInfo);
        }
        return playerInfo;
    }

    private boolean playerNotMoved(PlayerMoveEvent playerMoveEvent) {
        return ((playerMoveEvent.getFrom().getX() == playerMoveEvent.getTo().getX()) &&
                (playerMoveEvent.getFrom().getZ() == playerMoveEvent.getTo().getZ())) ||
                (playerMoveEvent.getFrom().getYaw() == playerMoveEvent.getTo().getYaw());
    }

    private boolean isPlayerAfk(Player player) {
        boolean afk = checkVehicle(player);
        if (!afk && player.isInWater()) {
            afk = checkWaterAfk(player);
        } else if (waterTimeout != null && LocalDateTime.now().minusSeconds(5).isBefore(waterTimeout)) {
            afk = true;
        }
        // piston duwen


        return afk;
    }

    private boolean checkVehicle(Player player) {
        var vehicle = player.getVehicle();
        if (vehicle == null) {
            return false;
        }
        if (vehicle instanceof Minecart) {
            return true;
        }
        return vehicle.isInWater();
    }

    private boolean checkWaterAfk(Player player) {
        boolean afk;
        if (player.isInBubbleColumn()) {
            waterTimeout = LocalDateTime.now();
            afk = true;
        } else {
            afk = !isPlayerSwimming(player);
        }
        return afk;
    }

    private boolean isPlayerSwimming(Player player) {
        var vector = player.getVelocity();
        var swimming = false;
        if (player.isInWater() && vector.getX() == 0.0 && vector.getZ() == 0.0) {
            swimming = true;
        } else {
            waterTimeout = LocalDateTime.now();
        }
        return player.isSwimming() || swimming;
    }
}
