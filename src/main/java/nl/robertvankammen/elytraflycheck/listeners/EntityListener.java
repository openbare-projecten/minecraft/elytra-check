package nl.robertvankammen.elytraflycheck.listeners;

import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

import static org.bukkit.enchantments.Enchantment.DURABILITY;

public class EntityListener implements Listener {
    private static final Set<Material> SHULKERS = Tag.SHULKER_BOXES.getValues();

    @EventHandler
    public void onPickup(EntityPickupItemEvent event) {
        if (event.getEntity() instanceof HumanEntity player) {
            // invetory is van een speler kijk of er een shulker box in de hot bar is
            for (var i = 0; i < 9; i++) {
                var item = player.getInventory().getItem(i);
                if (item != null && SHULKERS.contains(item.getType())) {
                    var sucked = checkShulker(item, event.getItem());
                    if (sucked) { // verwijder het opgepakte item uit de invetory van de player
                        player.getInventory().remove(event.getItem().getItemStack());
                        event.getItem().remove();
                        event.setCancelled(true);
                        break;
                    }
                }
            }
        }
    }

    private boolean checkShulker(ItemStack hotbarItem, @NotNull Item eventItem) {
        if (hotbarItem.getItemMeta() instanceof BlockStateMeta itemMeta) {
            if (itemMeta.getBlockState() instanceof ShulkerBox shulkerBox) {
                var firstSlotItem = shulkerBox.getInventory().getItem(0);
                if (firstSlotItem == null) {
                    return false;
                }
                if (firstSlotItem.getType().equals(Material.ENCHANTED_BOOK)) {
                    // blijkbaar wordt de enchant niet goed opgeslagen in de properties dus dan maar op tekst van de lore
                    var shulkerSucker = firstSlotItem.getItemMeta().lore().stream()
                            .map(component -> component.contains(Component.text("Shulker Sucker 1")))
                            .findAny().isPresent();
                    if (shulkerSucker) { // shulker sucker is gevonden, check de 2e slot welk item dat is
                        var secondSlotItem = shulkerBox.getInventory().getItem(1);
                        if (secondSlotItem == null) {
                            return false;
                        }
                        if (secondSlotItem.getType().equals(eventItem.getItemStack().getType())) {
                            hotbarItem.addUnsafeEnchantment(DURABILITY, 1);
                            // zelfde item dus opzuigen
                            var amountEventItem = eventItem.getItemStack().getAmount();
                            var overige = shulkerBox.getInventory().addItem(eventItem.getItemStack());
                            itemMeta.setBlockState(shulkerBox);
                            hotbarItem.setItemMeta(itemMeta);
                            // overige paste niet, dus dat op de grond droppen
                            if (!overige.isEmpty()) {
                                var overigeItems = overige.get(0);
                                if (overigeItems.getAmount() == amountEventItem) {
                                    // shulker is vol
                                    return false;
                                }
                                eventItem.getWorld().dropItem(eventItem.getLocation(), overigeItems);
                                return true;
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
