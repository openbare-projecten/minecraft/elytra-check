package nl.robertvankammen.elytraflycheck.listeners;

import net.kyori.adventure.text.Component;
import nl.robertvankammen.elytraflycheck.Main;
import nl.robertvankammen.elytraflycheck.domain.PlayerInfo;
import nl.robertvankammen.elytraflycheck.domain.ShopInfo;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static org.bukkit.Material.*;
import static org.bukkit.enchantments.Enchantment.*;

public class InvetoryListener implements Listener {
    private static final Set<Material> TOEGESTANE_TOOLS = Set.of(WOODEN_PICKAXE, STONE_PICKAXE, GOLDEN_PICKAXE, IRON_PICKAXE, DIAMOND_PICKAXE, NETHERITE_PICKAXE);
    private static final Map<Enchantment, Integer> ENCHANTMENTS = Map.of(DURABILITY, 4, DIG_SPEED, 7, LOOT_BONUS_BLOCKS, 4);
    private final List<ShopInfo> shopConfig;

    public InvetoryListener(List<ShopInfo> shopConfig) {
        this.shopConfig = shopConfig;
    }

    @EventHandler
    public void onInvetoryClick(InventoryClickEvent inventoryClickEvent) {
        var clickedInvetory = inventoryClickEvent.getClickedInventory();
        var player = inventoryClickEvent.getWhoClicked();
        var inv = Main.PLAYER_SHOPS.get(player.getUniqueId());
        if (inv != null && inv.equals(clickedInvetory)){
            inventoryClickEvent.setCancelled(true);
            shopConfig.stream()
                    .filter(shopInfo -> shopInfo.getInvLocation().equals(inventoryClickEvent.getSlot()))
                    .findAny()
                    .ifPresent(shopInfo -> buyItem(shopInfo, player));
            clickedInvetory.close();
        }
    }

    @EventHandler
    public void onAnvil(PrepareAnvilEvent anvilEvent) {
        var firstItem = anvilEvent.getInventory().getFirstItem();
        var secondItem = anvilEvent.getInventory().getSecondItem();
        // alleen voor toegestane tools
        if (firstItem != null && TOEGESTANE_TOOLS.contains(firstItem.getType())) {
            if (secondItem != null && secondItem.getType() == ENCHANTED_BOOK) {
                var returnItem = checkBetterEnchants(firstItem, secondItem);
                if (returnItem != null) {
                    anvilEvent.setResult(returnItem);
                    anvilEvent.getInventory().setRepairCost(39);
                }
            }
        }
    }

    private ItemStack checkBetterEnchants(ItemStack firstItem, ItemStack secondItem) {
        var resultItem = firstItem.clone();
        AtomicReference<Enchantment> selectedEnchant = new AtomicReference<>();
        AtomicReference<Integer> level = new AtomicReference<>(0);
        secondItem.getEnchantments().forEach((enchantment, integer) -> {
            var x = ENCHANTMENTS.get(enchantment);
            if (x != null) {
                selectedEnchant.set(enchantment);
                level.set(x);
            }
        });
        if (selectedEnchant.get() != null) {
            resultItem.addUnsafeEnchantment(selectedEnchant.get(), level.get());
            return resultItem;
        } else {
            return null;
        }
    }

    private void buyItem(ShopInfo shopInfo, HumanEntity player) {
        if (shopInfo.getKostenInDiamonds() != null){
            var itemMainHand = player.getInventory().getItemInMainHand();
            if (itemMainHand.getType().equals(DIAMOND) && itemMainHand.getAmount() == shopInfo.getKostenInDiamonds()) {
                createItem(player, shopInfo.getItem());
                player.getInventory().setItemInMainHand(null);
                Bukkit.broadcast(Component.text(String.format("%s heeft een %s gekocht.", player.getName(), shopInfo.getNaam())));
            } else {
                player.sendMessage(Component.text("Je moet " + shopInfo.getKostenInDiamonds() + " diamonds vast houden"));
            }
        } else if(shopInfo.getKostenInMinuten() != null){
            var playerInfo = Main.PLAYERS.get(player.getUniqueId().toString());
            var minuten = playerInfo.getMinutenElytraNietGebruikt();
            // de anvil die kost 5
            if (minuten >= shopInfo.getKostenInMinuten()) {
                playerInfo.setMinutenElytraNietGebruikt(minuten - shopInfo.getKostenInMinuten());
                createItem(player, shopInfo.getItem());
                Bukkit.broadcast(Component.text(String.format("%s heeft een %s gekocht.", player.getName(), shopInfo.getNaam())));
                if (shopInfo.getNaam().equals("Elytra")) {
                    var item2 = new ItemStack(Material.TALL_GRASS);
                    item2.lore(List.of(Component.text("Je bent al zo lang op de grond dat je dit item wel verdient.")));
                    item2.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
                    createItem(player, item2);
                }
            } else {
                player.sendMessage(Component.text("Je hebt niet genoeg M, kosten zijn " + shopInfo.getKostenInMinuten() + " en je hebt " + minuten));
            }
        }
    }

    private void createItem(HumanEntity player, ItemStack itemStack) {
        var notPlaced = player.getInventory().addItem(itemStack);
        if (!notPlaced.isEmpty()) {
            player.getWorld().dropItem(player.getLocation(), itemStack);
        }
        spawnFireworks(player.getLocation(), 5);
    }

    public static void spawnFireworks(Location loc, int amount) {
        var fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
        var fwm = fw.getFireworkMeta();

        fwm.setPower(1);
        fwm.addEffect(FireworkEffect.builder()
                .withColor(Color.BLUE)
                .withColor(Color.LIME)
                .flicker(true)
                .withTrail()
                .build());

        fw.setFireworkMeta(fwm);
        //fw.detonate();

        for (int i = 0; i < amount; i++) {
            var fw2 = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
            fw2.setFireworkMeta(fwm);
        }
    }
}
