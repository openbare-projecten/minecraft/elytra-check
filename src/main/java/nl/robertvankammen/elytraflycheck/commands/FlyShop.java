package nl.robertvankammen.elytraflycheck.commands;

import nl.robertvankammen.elytraflycheck.domain.ShopInfo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;

import static net.kyori.adventure.text.Component.text;
import static nl.robertvankammen.elytraflycheck.Main.PLAYERS;
import static org.bukkit.event.inventory.InventoryType.CHEST;

public class FlyShop {

    public Inventory openNoFlyShop(Player player, List<ShopInfo> shopInfo) {
        var inv = Bukkit.createInventory(null, CHEST, text("Buy your shit. Saldo: " + PLAYERS.get(player.getUniqueId().toString()).getMinutenElytraNietGebruikt() + "M"));
        shopInfo.forEach(info -> {
                    inv.setItem(info.getInvLocation(), info.getItem());
                });
        player.openInventory(inv);
        return inv;
    }
}
