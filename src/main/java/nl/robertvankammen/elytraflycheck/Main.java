package nl.robertvankammen.elytraflycheck;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import nl.robertvankammen.elytraflycheck.commands.FlyShop;
import nl.robertvankammen.elytraflycheck.domain.PlayerInfo;
import nl.robertvankammen.elytraflycheck.domain.ShopInfo;
import nl.robertvankammen.elytraflycheck.enchantment.CustomEnchant;
import nl.robertvankammen.elytraflycheck.listeners.EntityListener;
import nl.robertvankammen.elytraflycheck.listeners.InvetoryListener;
import nl.robertvankammen.elytraflycheck.listeners.PlayerAfkListener;
import nl.robertvankammen.elytraflycheck.listeners.PlayerNpcListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static net.kyori.adventure.text.Component.text;
import static nl.robertvankammen.elytraflycheck.domain.ConfigLocationEnum.*;
import static org.bukkit.Material.ENCHANTED_BOOK;
import static org.bukkit.enchantments.Enchantment.*;

public class Main extends JavaPlugin implements Listener {
    private static final String COMMAND_STRING_TIME = "noflytime";
    private static final String COMMAND_STRING_SET_LOCATION = "setlocation";

    public static final Map<String, PlayerInfo> PLAYERS = new HashMap<>();
    public static final Map<UUID, Inventory> PLAYER_SHOPS = new HashMap<>();

    @Override
    public void onEnable() {
        ConfigurationSerialization.registerClass(PlayerInfo.class);
        ConfigurationSerialization.registerClass(ShopInfo.class);
        init();
        var shopConfig = (List<ShopInfo>) getConfig().get(SHOP_DATA_ROOT.getLocatie(), List.class);
        this.getServer().getPluginManager().registerEvents(new PlayerAfkListener(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerNpcListener(this, new FlyShop(), shopConfig, getConfig().getLocation(LOCATION_NPC.getLocatie())), this);
        this.getServer().getPluginManager().registerEvents(new InvetoryListener(shopConfig), this);
        this.getServer().getPluginManager().registerEvents(new EntityListener(), this);
        CustomEnchant.register();
    }

    private void init() {
        initConfig();
        var listConfig = (List<PlayerInfo>) getConfig().getList(PLAYER_DATA_ROOT.getLocatie());
        if (listConfig != null) {
            listConfig.forEach(playerInfo -> PLAYERS.put(playerInfo.getUuid(), playerInfo));
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::checkPlayersAfk, 1, 100);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::savePluginConfig, 1, 20 * 60);
    }

    private void initConfig() {
        if (getConfig().get(CONFIG_VERSION.getLocatie()) == null) {
            getConfig().set(SHOP_DATA_ROOT.getLocatie(), defaultShop());
            getConfig().set(CONFIG_VERSION.getLocatie(), 1);
            saveConfig();
        }
        if ((Integer)getConfig().get(CONFIG_VERSION.getLocatie(), Integer.class) == 1){
            getConfig().set(LOCATION_NPC.getLocatie(), new Location(null, 0, 300, 0));
            getConfig().set(CONFIG_VERSION.getLocatie(), 2);
            saveConfig();
        }
        // todo add safty checks if somebody messed up the config
    }

    private void savePluginConfig() {
        var config = PLAYERS.values().stream().toList();
        getConfig().set(PLAYER_DATA_ROOT.getLocatie(), config);
        saveConfig();
    }

    private void checkPlayersAfk() {
        Bukkit.getOnlinePlayers().stream()
                .map(player -> PLAYERS.get(player.getUniqueId().toString()))
                .forEach(playerInfo -> {
                    if (playerInfo.getLastNonAfkMoveEvent().plusSeconds(3).isAfter(LocalDateTime.now())) {
                        playerInfo.addAantalSeconden(5);
                    }
                });
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(COMMAND_STRING_TIME) && sender instanceof Player player) {
            sender.sendMessage(text("Aantal minuten elytra niet gebruikt: " + PLAYERS.get(player.getUniqueId().toString()).getMinutenElytraNietGebruikt()));
        }
        if (label.equalsIgnoreCase(COMMAND_STRING_SET_LOCATION) && sender instanceof Player player) {
            if (player.isOp()) {
                var location = player.getLocation();
                getConfig().set(LOCATION_NPC.getLocatie(), new Location(null, location.x(), location.y(), location.z()));
                sender.sendMessage(text("Locatie geupdated, een herstart is nodig"));
                saveConfig();
            } else {
                sender.sendMessage(text("Alleen een admin mag deze command runnen"));
            }
        }
        return false;
    }

    private static final String LORE_TEXT = "Only for pickaxes";

    private List<ShopInfo> defaultShop() {
        var anvil = new ShopInfo("Anvil", 10, null, 10, new ItemStack(Material.ANVIL));
        var gold = new ShopInfo("Gold nugget", 20, null, 11, new ItemStack(Material.GOLD_NUGGET, 64));
        var redstone = new ShopInfo("Redstone dust", 40, null, 12, new ItemStack(Material.REDSTONE, 64));
        var bookUnbr4 = new ItemStack(ENCHANTED_BOOK);
        bookUnbr4.addUnsafeEnchantment(DURABILITY, 4);
        bookUnbr4.lore(List.of(Component.text(LORE_TEXT)));
        var unbreaking4 = new ShopInfo("Unbreaking 4", 1250, null, 13, bookUnbr4);
        var bookEff7 = new ItemStack(ENCHANTED_BOOK);
        bookEff7.addUnsafeEnchantment(DIG_SPEED, 7);
        bookEff7.lore(List.of(Component.text(LORE_TEXT)));
        var eff7 = new ShopInfo("Efficiency 7", 1250, null, 14, bookEff7);
        var bookFortune4 = new ItemStack(ENCHANTED_BOOK);
        bookFortune4.addUnsafeEnchantment(LOOT_BONUS_BLOCKS, 4);
        bookFortune4.lore(List.of(Component.text(LORE_TEXT)));
        var fortune4 = new ShopInfo("Fortune 4", 1250, null, 15, bookFortune4);
        var elytraItem = new ItemStack(Material.ELYTRA);
        var elytraMeta = elytraItem.getItemMeta();
        elytraMeta.setUnbreakable(true);
        elytraItem.setItemMeta(elytraMeta);
        elytraItem.lore(List.of(Component.text("No Fly Elytra")));
        var elytra = new ShopInfo("Elytra", 3000, null, 16, elytraItem);
        var bookShulkerSucker = new ItemStack(ENCHANTED_BOOK);
        bookShulkerSucker.addUnsafeEnchantment(CustomEnchant.SHULKER_SUCKER, 1);

        bookShulkerSucker.lore(List.of(
                text("Shulker Sucker 1").style(Style.style()
                        .color(TextColor.color(210, 210, 210))
                        .decorate(TextDecoration.BOLD)
                        .build()),
                text("Plaats dit boek in de"),
                text("eerste slot van je shulker"),
                text("en plaats de item die je"),
                text("op wilt laten zuigen in het 2e slot."),
                text("Hou de shulkerbox in je hotbar"),
                text("en zie de shulker boks zuigen ")
        ));
        var shulkerSucker = new ShopInfo("Shulker sucker", null, 64, 22, bookShulkerSucker);
        var shulkerSpawnEgg = new ShopInfo("Shulker Spawn Egg", 1000, null, 4, new ItemStack(Material.SHULKER_SPAWN_EGG));

        return List.of(anvil, gold, redstone,unbreaking4,eff7,fortune4, elytra, shulkerSucker, shulkerSpawnEgg);
    }
}
