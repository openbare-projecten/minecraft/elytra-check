package nl.robertvankammen.elytraflycheck.domain;

public enum ConfigLocationEnum {
  CONFIG_VERSION("config.version", "Config version"),
  LOCATION_NPC("npc.location", "Npc location version"),
  PLAYER_DATA_ROOT("data", "De lijst voor alle player data"),
  SHOP_DATA_ROOT("shop", "De lijst voor alle shop data");

  private final String locatie;
  private final String omschrijving;

  ConfigLocationEnum(String locatie, String omschrijving) {
    this.locatie = locatie;
    this.omschrijving = omschrijving;
  }

  public String getLocatie() {
    return locatie;
  }
}
