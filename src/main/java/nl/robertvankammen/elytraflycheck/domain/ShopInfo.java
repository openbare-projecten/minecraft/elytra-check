package nl.robertvankammen.elytraflycheck.domain;

import net.kyori.adventure.text.Component;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopInfo implements ConfigurationSerializable {
    static {
        ConfigurationSerialization.registerClass(ShopInfo.class);
    }

    private String naam;
    private Integer kostenInMinuten;
    private Integer kostenInDiamonds;
    private Integer invLocation;
    private ItemStack item;

    // serialise voor plugin
    @Override
    public @NotNull Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("naam", naam);
        data.put("kostenInMinuten", kostenInMinuten);
        data.put("kostenInDiamonds", kostenInDiamonds);
        data.put("invLocation", invLocation);
        data.put("item", item);
        return data;
    }

    // deserialise voor plugin
    public ShopInfo(Map<String, Object> data) {
        naam = (String) data.get("naam");
        kostenInMinuten = (Integer) data.get("kostenInMinuten");
        kostenInDiamonds = (Integer) data.get("kostenInDiamonds");
        invLocation = (Integer) data.get("invLocation");
        item = (ItemStack) data.get("item");
    }

    public ShopInfo(String naam, Integer kostenInMinuten, Integer kostenInDiamonds, Integer invLocation, ItemStack item) {
        this.naam = naam;
        this.kostenInMinuten = kostenInMinuten;
        this.kostenInDiamonds = kostenInDiamonds;
        this.invLocation = invLocation;
        this.item = item;
    }

    public String getNaam() {
        return naam;
    }

    public Integer getKostenInMinuten() {
        return kostenInMinuten;
    }

    public Integer getKostenInDiamonds() {
        return kostenInDiamonds;
    }

    public Integer getInvLocation() {
        return invLocation;
    }

    public ItemStack getItem() {
        var clonedItem = item.clone();
        var metadata = clonedItem.getItemMeta();
        var lore = metadata.lore();
        if (lore == null){
            lore = new ArrayList<>();
        }
        if (kostenInDiamonds != null){
            lore.add(Component.text(String.format("Kosten: %s diamonds", kostenInDiamonds)));
        }
        if (kostenInMinuten != null){
            lore.add(Component.text(String.format("Kosten: %s minuten", kostenInMinuten)));
        }
        metadata.lore(lore);
        clonedItem.setItemMeta(metadata);
        return clonedItem;
    }
}
