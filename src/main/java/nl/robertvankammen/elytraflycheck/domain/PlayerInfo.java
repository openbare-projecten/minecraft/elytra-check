package nl.robertvankammen.elytraflycheck.domain;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class PlayerInfo implements ConfigurationSerializable {
    static {
        ConfigurationSerialization.registerClass(PlayerInfo.class);
    }

    private LocalDateTime lastNonAfkMoveEvent;
    private Integer minutenElytraNietGebruikt;
    private Integer seconden;
    private String uuid;

    // serialise voor plugin
    @Override
    public @NotNull Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("minutenElytraNietGebruikt", minutenElytraNietGebruikt);
        data.put("seconden", seconden);
        data.put("uuid", uuid);
        return data;
    }

    // deserialise voor plugin
    public PlayerInfo(Map<String, Object> data) {
        minutenElytraNietGebruikt = (Integer) data.get("minutenElytraNietGebruikt");
        seconden = (Integer) data.get("seconden");
        uuid = (String) data.get("uuid");
    }

    public PlayerInfo(String uuid) {
        this.minutenElytraNietGebruikt = 0;
        this.seconden = 0;
        this.uuid = uuid;
    }

    public void addAantalSeconden(int aantal) {
        seconden += aantal;
        if (seconden > 60) {
            seconden -= 60;
            minutenElytraNietGebruikt++;
        }
    }

    public void resetElytra() {
        seconden = 0;
        minutenElytraNietGebruikt = 0;
    }

    public String toString() {
        return "seconden: " + seconden + " minuten: " + minutenElytraNietGebruikt;
    }

    public LocalDateTime getLastNonAfkMoveEvent() {
        return lastNonAfkMoveEvent;
    }

    public void setLastNonAfkMoveEvent(LocalDateTime lastNonAfkMoveEvent) {
        this.lastNonAfkMoveEvent = lastNonAfkMoveEvent;
    }

    public Integer getMinutenElytraNietGebruikt() {
        return minutenElytraNietGebruikt;
    }

    public void setMinutenElytraNietGebruikt(Integer minutenElytraNietGebruikt) {
        this.minutenElytraNietGebruikt = minutenElytraNietGebruikt;
    }

    public Integer getSeconden() {
        return seconden;
    }

    public void setSeconden(Integer seconden) {
        this.seconden = seconden;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
