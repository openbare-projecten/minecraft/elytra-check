package nl.robertvankammen.elytraflycheck.enchantment;

import org.bukkit.enchantments.Enchantment;

import java.lang.reflect.Field;
import java.util.Arrays;

public class CustomEnchant {
    public static final Enchantment SHULKER_SUCKER = new ShulkerSucker("shulkersucker");

    public static void register() {
        boolean registered = Arrays.stream(Enchantment.values()).toList().contains(SHULKER_SUCKER);

        if (!registered)
            registerEnchantment(SHULKER_SUCKER);
    }

    public static void registerEnchantment(Enchantment enchantment) {
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
            Enchantment.registerEnchantment(enchantment);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
